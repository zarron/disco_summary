# Summary

## Concepts
### Coloring
- $`\delta`$ is the degree of a node
- $`\Delta`$ is the maximum degree of any node in the tree
- $`\Chi`$ is the Chromatic Number, i.e. the least number of colors 
- $`\log*(x)`$ how many times $`\log`$ has to be applied to x until it becomes 2
- $`\Delta`$ Cover Free Families: A set of sets, where a set $`i`$ is not a a subset of the union of $`\Delta`$ other sets

### Graphs
- Radius (Node): The maximum distance between a node and the one that is the furthest away from it
- Radius (Graph): The minimum radius of a node in the graph
- Diameter: The maximum distance between any two nodes in the graph 

## Theorems
- 1.13: $`\Chi(Tree)\leq2`$
- 4.1: Any LOCAL algorithm for 2-coloring an n-node directed path requires at least $`\Omega(n)`$
rounds.
- 4.2: Any deterministic algorithm for 3-coloring n-node directed paths needs at least $`\frac{\log∗ n}{2}− 2`$
rounds.
- 8.4: If an oblivious comparison-exchange algorithm sorts all inputs of 0’s and 1’s, then it sorts arbitrary inputs.
## Algorithms

### Coloring

|Name|Algorithm|Round Complexity|Colors|
|---|---|---|---|
|Greedy Sequential|![greedy sequential](images/greedy_sequential.png)|$`O(n)`$|$`\Delta+1`$|
|Reduce|![reduce](images/reduce.png)|$`O(n)`$|$`\Delta+1`$|
|Slow Tree Coloring|![slow_tree_coloring](images/slow_tree_coloring.png)|$`O(Tree Height)`$|$`2`$|
|6-Color|![6_color](images/6_color.png)|$`O(\log*(n))`$|$`6`$|
|Six-2-Three|![shift down](images/shift_down.png)![six-2-three](images/six_2_three.png)|$`O(1)`$|$`3`$|
|Linial|<ul><li>Assign a set of a $`\Delta+1`$ cover free family to each node</li><li>Choose an ID for this node that is not contained in the sets of its neighbors</li></ul>|$`O(1)`$|$`\Delta+1`$|
|MIS|![mis](images/mis.png)<lu><li>Create $`\Delta+1`$ copies of the graph</li><li>Calculate MIS</li><li>If $`i^{th}`$ copy is in MIS, color vertex with color $`i`$</li></lu>|$`O(n)`$|$`\Delta+1`$|

### Trees

|Name|Algorithm|Round Complexity|Message Complexity|
|---|---|---|---|
|Flooding|![flooding](images/flooding.png)|$`O(n)`$|$`O(m)`$|
|Echo|![echo](images/echo.png)|$`O(n)`$|$`O(n)`$|
|Dijkstra BFS|![dijkstra_bfs](images/dijkstra_bfs.png)|$`O(D^2)`$|$`O(m+nD)`$|
|Bellman-Ford BFS|![bellman_ford_bfs](images/bellman_ford_bfs.png)|$`O(D)`$|$`O(nm)`$|
|GHS MST|![ghs](images/ghs.png)|$`O(n\log n)`$|$`O(m\log n)`$|
|Luby’s MIS|![luby](images/luby.png)|$`O(\log n)`$ w.h.p|$`O(m\log n)`$ w.h.p|

### Sharing
|Name|Algorithm|
|---|---|
|Centralized Solution|![centralized](images/centralized.png)|
|Home-Based Solution|![home](images/home.png)|
|Arrow|<ul><li>object is saved at node that last accessed it, this is the parent</li><li>find requests can be sent</li><li>find request reverses parent child relationships</li><li>any node that has the object sends it to its parent</li></ul>|
|Caching|![caching](images/caching.png)|
|Pointer Forwarding|![pointer_forwarding](images/pointer_forwarding.png)|
|Ivy|![ivy](images/ivy.png)|

### Sorting
|Name|Algorithm|Steps|
|---|---|---|
|Odd/Even|![odd_even](images/odd_even.png)|$`O(n)`$|
|Shear|![shear](images/shear.png)|$`O(\sqrt{n}\log(n))`$|

### Sorting Networks
|Name|Algorithm|Dept|
|---|---|---|
|Half Cleaner|![half_cleaner](images/half_cleaner.png)<br>Feeding a bitonic sequence into a half cleaner, the half cleaner cleans (makes all-0 or all-1) either the upper or the lower half of the n wires. The other half is bitonic.|$`n`$|
|Bitonic Sequence Sorter|![bitonic_sequence_sorter](images/bitonic_sequence_sorter.png)<br>A bitonic sequence sorter of width n sorts bitonic sequences. It has depth log n|$`\log n`$|
|Merging Network|![merging_network](images/merging_network.png)<br>A merging network of width n merges two sorted input sequences of length n/2 each into one sorted sequence of length n.|$`1+2\log n`$|
|Batcher Network|![batcher](images/batcher.png)<br>A sorting network (Algorithm 8.18) sorts an arbitrary sequence of n values.|$`O(\log^2n)`$|

### Advanced
|Name|Algorithm|Rounds|
|---|---|---|
|Naive Diameter Construction|![naive_diameter](images/naive_diameter.png)|$`O(D)`$|
|APSP Diameter Construction|![diameter_synchronous](images/diameter_synchronous.png)|$`O(n)`$|
|Borůvka's Algorithm|<ul><li>Start with empty spanning forest</li><li>Each fragment computes minimal outgoing edge</li><li>Fragments get merged over minimal outgoing edges, and they get added to the MST</li><li>Repeat</li></ul>|$`O((D+\sqrt{n})\log n)`$|

### Definitions
|Name|Definition|
|---|---|
|Communication Complexity|![communication_complexity](images/communication_complexity.png)|
|Monochromatic|![monochromatic](images/monochromatic.png)|
|Fooling Set|![](images/fooling_set.png)|

### Wireless Protocols
|Name|Algorithm|CD|n known|
|---|---|---|---|
|Slotted Aloha|![slotted_aloha](images/slotted_aloha.png)||:heavy_check_mark:|
|Uniform Initialization with CD|![uniform_initialization_with_cd](images/uniform_initialization_with_cd.png)|:heavy_check_mark:|:x:|
|Fake CD|![fake_CD](images/fake_CD.png)|:x:|:x:|
|Uniform Leader Election|![uniform_leader_election](images/uniform_leader_election.png)|:x:|:x:|
|Uniform Leader Election with CD|![uniform_leader_election_CD](images/uniform_leader_election_CD.png)|:heavy_check_mark:|:x:|

### Labeling
|Name|Algorithm|Label Size|
|---|---|---|
|Naive Distance Labeling|![naive_distance_labeling](images/naive_distance_labelin.png)|$`O(n\log n)`$|
|Heavy Light Decomposition|![heavy_light_decomposition](images/heavy_light_decomposition.png)|$`O(\log^2n)`$|
|Naive Hub Labeling|![naive_hub_labeling](images/naive_hub_labeling.png)|$`O(\mid H\mid \log n)`$|
|Hub Labeling|![hub_labeling](images/hub_labeling.png)|$`O(\mid F\mid)\log n`$|

## Structures
- TODO: Local Model
- TODO: Congest Model

## Varia

### Landauer Notation

|Symbol|Meaning|
|---|---|
|$`f \in O(g)`$|f "smaller/equal" than g|
|$`f \in \Theta(g)`$|g and f are similar|
|$`f \in \Omega(g)`$|f is "larger/equal" than g|

### Inequalities

|Name|Inequality|
|---|---|
|Boole|$`Pr[\bigcup_i E_i] \leq \sum_i Pr[E_i]`$, where $`E_i`$ are events|
|Markov|$`Pr[\mid X \mid \geq a] \leq \frac{E[X]}{a}`$|
||$`1-p\leq(1-p/k)^k`$|
